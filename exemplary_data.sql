INSERT INTO hotel.role (name) values ('ORDINARY_USER');
INSERT INTO hotel.role (name) values ('ADMIN');

INSERT INTO hotel.room (number, size) values (10, 1);
INSERT INTO hotel.room (number, size) values (11, 1);
INSERT INTO hotel.room (number, size) values (20, 2);
INSERT INTO hotel.room (number, size) values (30, 3);
INSERT INTO hotel.room (number, size) values (40, 4);
INSERT INTO hotel.room (number, size) values (41, 4);