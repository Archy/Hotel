CREATE SCHEMA `hotel` ;
CREATE USER 'hotel'@'localhost' IDENTIFIED BY 'hotel';
GRANT ALL PRIVILEGES ON hotel.* TO 'hotel'@'localhost';
FLUSH PRIVILEGES;