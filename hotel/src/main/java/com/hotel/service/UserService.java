package com.hotel.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hotel.data.RoleRepository;
import com.hotel.data.UserRepository;
import com.hotel.model.Role;
import com.hotel.model.User;
import com.hotel.model.constraint.UniqueEmailValidator;

/**
 * Service for adding users to database
 * 
 * @author Janek
 *
 */
@Service
public class UserService {
	private static final String DEFAULT_ROLE = "ORDINARY_USER";
	@Autowired
	private UserRepository userRepository;
	@Autowired
	private RoleRepository roleRepository;
	
	 public void addWithDefaultRole(User user) {
        Role defaultRole = roleRepository.findByName(DEFAULT_ROLE);
        user.setUserRole(defaultRole);
        
        UniqueEmailValidator.insertValidation = true;
        user = userRepository.saveAndFlush(user);
        UniqueEmailValidator.insertValidation = false; 
    }
}