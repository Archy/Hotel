package com.hotel.service;

import java.sql.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hotel.data.ReservationRepository;
import com.hotel.data.RoomRepository;
import com.hotel.data.UserRepository;
import com.hotel.model.Reservation;
import com.hotel.model.Room;
import com.hotel.properties.ReservationProperties;

/**
 * Service for rooms reservation
 * 
 * @author Janek
 *
 */
@Service
public class ReservationService {
	@Autowired
	private ReservationProperties reservProp;
	@Autowired
	private UserRepository userRepo;
	@Autowired
	private RoomRepository roomRepo;
	@Autowired
	private ReservationRepository reservationRepo;
	
	public boolean reserveRooms(String userEmail, Date startingDate, Date endDate, List<Integer> orderedQuntities){	
		Reservation rsrv = new Reservation(startingDate, endDate);	
		for(int i=0; i<reservProp.getMaxRoomSize(); ++i){
			List<Room> freeRooms = roomRepo.findFreeRooms(startingDate, endDate, i+1);
			if(freeRooms.size() < orderedQuntities.get(i)){
				return false;
			}
			
			for(int j=0; j<orderedQuntities.get(i); ++j){
				rsrv.getRooms().add(freeRooms.get(j));
			}
		}
		//enough free rooms to fulfill order -> save in database
		rsrv.setUser(userRepo.findByEmail(userEmail));
		reservationRepo.save(rsrv);
		
		return true;
	}
}
