package com.hotel.service;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.hotel.data.UserRepository;

/**
 * Service for obtaining UserRepository
 * 
 * @author Janek
 *
 */
@Component
public class ServiceUtils {
  private static ServiceUtils instance;

  @Autowired
  private UserRepository userService;

  
  @PostConstruct
  public void fillInstance() {
    instance = this;
  }


  public static UserRepository getUserService() {
    return instance.userService;
  }
}