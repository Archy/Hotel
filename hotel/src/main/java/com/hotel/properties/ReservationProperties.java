package com.hotel.properties;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * Allows to obtain reservation related data from aplication.properties file
 * 
 * @author Janek
 *
 */
@ConfigurationProperties("hotel.reservation")
@Component
public class ReservationProperties {
	private int maxRoomSize;

	public int getMaxRoomSize() {
		return maxRoomSize;
	}

	public void setMaxRoomSize(int maxRoomSize) {
		this.maxRoomSize = maxRoomSize;
	}
}
