package com.hotel.model.constraint;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import com.hotel.data.UserRepository;
import com.hotel.service.ServiceUtils;

/**
 * Class for checking email uniqueness
 * 
 * @author Janek
 *
 */
public class UniqueEmailValidator implements ConstraintValidator<UniqueEmail, String>{

	private UserRepository userRepository;
	public static boolean insertValidation = false;
		
	public UniqueEmailValidator() {
	}
	
	@Override
	public void initialize(UniqueEmail email) {
		userRepository = ServiceUtils.getUserService();
	}

	@Override
	public boolean isValid(String email, ConstraintValidatorContext arg1) {
		return insertValidation || userRepository.checkMailUniqueness(email) == 0;
	}
}
