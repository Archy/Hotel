package com.hotel.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

/**
 * Class that represents users roles
 * 
 * @author Janek
 *
 */
@Entity
public class Role {
	@Id
	@GeneratedValue
	private long id;
	@Column(nullable = false, unique = true)
	private String name;
	
	public Role() {}

	/////////////////////////////////////////
	//getters and setters
	/////////////////////////////////////////
	
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
}
