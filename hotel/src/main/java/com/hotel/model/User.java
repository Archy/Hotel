package com.hotel.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import org.springframework.security.crypto.password.PasswordEncoder;

import com.hotel.model.constraint.UniqueEmail;

/**
 * Class that represents a user
 * 
 * @author Janek
 *
 */
@Entity
public class User {
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private long id;
	@UniqueEmail
	private String email;
	private String name;
	private String surname;
    private String password;
	@ManyToOne
	@JoinColumn(name="ROLE_ID")
	private Role userRole;
	
	public User(){}
	
	public void encodePassword(PasswordEncoder passEncode){
		password = passEncode.encode(password);
	}
	
	/////////////////////////////////////////
	//getters and setters
	/////////////////////////////////////////

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSurname() {
		return surname;
	}

	public void setSurname(String surname) {
		this.surname = surname;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public Role getUserRole() {
		return userRole;
	}

	public void setUserRole(Role userRole) {
		this.userRole = userRole;
	}
}
