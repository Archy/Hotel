package com.hotel.model;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToMany;

/**
 * Class that represents a hotel room 
 * 
 * @author Janek
 *
 */
@Entity
public class Room {
	@Id
	@GeneratedValue
	private long id;
	@Column(nullable = false)
	private int number;
	@Column(nullable = false)
	private int size;
	@ManyToMany(mappedBy = "rooms")
    private List<Reservation> reservations = new ArrayList<>();

	public Room(){}
	
	public Room(int number, int size){
		this.number = number;
		this.size = size;
	}

	/////////////////////////////////////////
	//getters and setters
	/////////////////////////////////////////

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public int getNumber() {
		return number;
	}

	public void setNumber(int number) {
		this.number = number;
	}

	public int getSize() {
		return size;
	}

	public void setSize(int size) {
		this.size = size;
	}	
	
	public List<Reservation> getProducts() {
		return reservations;
	}

	public void setProducts(List<Reservation> products) {
		this.reservations = products;
	}
	
	public List<Reservation> getReservations() {
		return reservations;
	}

	public void setReservations(List<Reservation> reservations) {
		this.reservations = reservations;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (int) (id ^ (id >>> 32));
		result = prime * result + number;
		result = prime * result + ((reservations == null) ? 0 : reservations.hashCode());
		result = prime * result + size;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Room other = (Room) obj;
		if (id != other.id)
			return false;
		if (number != other.number)
			return false;
		if (reservations == null) {
			if (other.reservations != null)
				return false;
		} else if (!reservations.equals(other.reservations))
			return false;
		if (size != other.size)
			return false;
		return true;
	}
}
