package com.hotel.model;

import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;

/**
 * Class that represents room(s) reservation
 * 
 * @author Janek
 *
 */
@Entity
public class Reservation {
	@Id
	@GeneratedValue
	private long id;
	@Column(nullable = false)
	private Date startingDate;
	@Column(nullable = false)
	private Date endDate;
	@ManyToOne
	@JoinColumn(name="USER_ID")
	private User user;
	@ManyToMany
    private List<Room> rooms = new ArrayList<Room>();
	
	public Reservation(){}
	
	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public Reservation(Date startingDate, Date endDate){
		this.startingDate = startingDate;
		this.endDate = endDate;
	}

	/////////////////////////////////////////
	//getters and setters
	/////////////////////////////////////////
	
	public long getId() {
		return id;
	}

	public List<Room> getRooms() {
		return rooms;
	}

	public void setRooms(List<Room> rooms) {
		this.rooms = rooms;
	}

	public void setId(long id) {
		this.id = id;
	}

	public Date getStartingDate() {
		return startingDate;
	}

	public void setStartingDate(Date startingDate) {
		this.startingDate = startingDate;
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}
}