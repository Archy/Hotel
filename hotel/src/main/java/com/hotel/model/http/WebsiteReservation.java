package com.hotel.model.http;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.hotel.properties.ReservationProperties;

/**
 * Class for obtaining reservation data from webiste
 * 
 * @author Janek
 *
 */
public class WebsiteReservation {
	private String rangeDate;
	private List<Integer> roomsQuantities;

	@Autowired
	private ReservationProperties reservProp;

	public WebsiteReservation(){
	}
	
	public WebsiteReservation(ReservationProperties reservProp) {
		this.reservProp = reservProp;
		roomsQuantities = new ArrayList<Integer>();
		for(int i=0; i< reservProp.getMaxRoomSize(); ++i){
			roomsQuantities.add(0);
		}
	}
	
	public WebsiteReservation(String rangeDate, List<Integer> roomsQuantities,
			ReservationProperties reservProp) {
		super();
		this.rangeDate = rangeDate;
		this.roomsQuantities = roomsQuantities;
		this.reservProp = reservProp;
	}

	public String getStartingDate(){
		return rangeDate.substring(0, 9);
	}
	
	public String getEndDate(){
		return rangeDate.substring(13);
	}
	
	/////////////////////////////////////////
	//getters and setters
	/////////////////////////////////////////
	
	public String getRangeDate() {
		return rangeDate;
	}

	public void setRangeDate(String rangeDate) {
		this.rangeDate = rangeDate;
	}

	public List<Integer> getRoomsQuantities() {
		return roomsQuantities;
	}

	public void setRoomsQuantities(List<Integer> roomsQuantities) {
		this.roomsQuantities = roomsQuantities;
	}

	public ReservationProperties getReservProp() {
		return reservProp;
	}

	public void setReservProp(ReservationProperties reservProp) {
		this.reservProp = reservProp;
	}
}
