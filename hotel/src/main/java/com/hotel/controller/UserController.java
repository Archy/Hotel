package com.hotel.controller;

import java.util.List;

import javax.validation.Valid;

import org.apache.log4j.spi.LoggerFactory;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

import com.hotel.model.User;
import com.hotel.service.UserService;

/**
 * Spring MVC controller of user authentication logic
 * 
 * @author Janek
 *
 */
@Controller
public class UserController {
	@Autowired
    private BCryptPasswordEncoder bcryptEncoder;
	@Autowired
	private UserService userService;

	@GetMapping("/registerform")
    public String register(Model model) {
        model.addAttribute("user", new User());
        return "register_form";
    }
 
    @PostMapping("/registerform")
    public String addUser(@Valid @ModelAttribute User user, BindingResult result) {
    	if (result.hasErrors()) {
            List<ObjectError> errors = result.getAllErrors();
            errors.forEach(err -> System.out.println(err.getDefaultMessage()));
            return "register_form";
        }
    	else{
    		user.encodePassword(bcryptEncoder);
        	userService.addWithDefaultRole(user);
        	return "redirect:/login_form";
    	}
    }
    
	@GetMapping("/loginform")
	public String loginForm() {
	    return "login_form";
	}
	
	@GetMapping("/loginform_failure")
	public String loginFailure() {
	    return "redirect:login_form";
	}
}
