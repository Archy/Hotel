package com.hotel.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import com.hotel.model.http.WebsiteReservation;
import com.hotel.properties.ReservationProperties;

/**
 * Spring MVC controller of main page
 * 
 * @author Janek
 *
 */
@Controller
public class IndexController {
	@Autowired
	private ReservationProperties reservProp;
	
	@GetMapping
	public String reserveGet(Model model) {
		model.addAttribute("reserv", new WebsiteReservation(this.reservProp));
		model.addAttribute("reservError", null);
	    return "index";
	}
		
	@GetMapping("/reserved")
    public String register() {
        return "reserved";
    }
}
