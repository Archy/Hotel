package com.hotel.controller;

import java.sql.Date;
import java.text.ParseException;
import java.text.SimpleDateFormat;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

import com.hotel.model.http.WebsiteReservation;
import com.hotel.properties.ReservationProperties;
import com.hotel.service.ReservationService;

/**
 * Spring MVC controller of room reservation logic
 * 
 * @author Janek
 *
 */
@Controller
public class ReservationController {	
	private static final Logger LOG = LoggerFactory.getLogger(IndexController.class);

	@Autowired
	private ReservationProperties reservProp;
	@Autowired
	private ReservationService reservService;
	
	
	@PostMapping
	public String reservePost(@ModelAttribute(name="reserv")  WebsiteReservation webReserv, Model model) {
		int sum=0;
		for(int i=0; i<reservProp.getMaxRoomSize(); ++i){
			sum += webReserv.getRoomsQuantities().get(i);
		}
		if(sum == 0){
			model.addAttribute("reservError", "Please, choose at least 1 room");
			return "index";
		}
		
		if(reserveRooms(webReserv)){
		    return "redirect:/reserved";
		} else {
			LOG.info("Room reservation not possible");
			model.addAttribute("reservError", "Sorry, not available");
			return "index";
		}
	}
	
	private boolean reserveRooms(WebsiteReservation webReserv){
		try {
			return reservService.reserveRooms(
					((User)SecurityContextHolder.getContext().getAuthentication().getPrincipal()).getUsername(), 
					stringToDate(webReserv.getStartingDate()), stringToDate(webReserv.getEndDate()), webReserv.getRoomsQuantities());
		} catch (ParseException e) {
			e.printStackTrace();
			LOG.error("string to date parsing error\n" + e);
			return false;
		}
	}
	
	private Date stringToDate(String str) throws ParseException{
		return new Date(new SimpleDateFormat("MM/dd/yyyy").parse(str).getTime());
	}
}
