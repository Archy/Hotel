package com.hotel.security;

import java.util.HashSet;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;

import com.hotel.data.UserRepository;
import com.hotel.model.User;

/**
 * Class for database based user authentication
 * 
 * @author Janek
 *
 */
@Component
public class HotelUserDetailsService implements UserDetailsService {
	
	@Autowired
	private UserRepository userRepository;
	
	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		User user = userRepository.findByEmail(username);
        if(user == null)
            throw new UsernameNotFoundException("User not found");
       
        Set<GrantedAuthority> grantedAuthority = new HashSet<>();
        grantedAuthority.add(new SimpleGrantedAuthority(user.getUserRole().getName()));
        
        org.springframework.security.core.userdetails.User userDetails = 
        		new org.springframework.security.core.userdetails.User(
        				user.getEmail(), user.getPassword(), grantedAuthority
        		);
        return userDetails;		
	}
}
