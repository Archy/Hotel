package com.hotel.security.logger;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationListener;
import org.springframework.security.authentication.event.AbstractAuthenticationEvent;
import org.springframework.security.authentication.event.AbstractAuthenticationFailureEvent;
import org.springframework.security.authentication.event.AuthenticationSuccessEvent;
import org.springframework.security.authentication.event.InteractiveAuthenticationSuccessEvent;
import org.springframework.stereotype.Component;

import com.hotel.controller.IndexController;

/**
 * Logging events listener
 * 
 * @author Janek
 *
 */
@Component
public class LoginListener implements ApplicationListener<AbstractAuthenticationEvent> {
	private static final Logger LOG = LoggerFactory.getLogger(IndexController.class);

    @Override
    public void onApplicationEvent(AbstractAuthenticationEvent event)
    {
    	if (event instanceof InteractiveAuthenticationSuccessEvent || event instanceof AuthenticationSuccessEvent) {
    		LOG.info("User: " + event.getAuthentication().getName() + " logged in");

        } else if (event instanceof AbstractAuthenticationFailureEvent) {
        	LOG.warn("User: " + event.getAuthentication().getName() + " tried to log in, but failed miserably");
        }
    }
}