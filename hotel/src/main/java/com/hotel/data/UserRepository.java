package com.hotel.data;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.hotel.model.User;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {
	User findByEmail(String email);
	@Query(nativeQuery = true, value = "SELECT count(*) FROM user WHERE email = ?")
	int checkMailUniqueness(String email);
}
