package com.hotel.data;

import java.sql.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.hotel.model.Room;

@Repository
public interface RoomRepository extends JpaRepository<Room, Long> {
	@Query("SELECT DISTINCT r FROM Room r WHERE r.size = :size AND r.id NOT IN ("
			+ "SELECT rr.id FROM Room rr JOIN rr.reservations rsrv WHERE rsrv.startingDate <= :endDate AND rsrv.endDate >= :startingDate"
			+ ")")
	List<Room> findFreeRooms( @Param("startingDate") Date startingDate, @Param("endDate") Date endDate, @Param("size") int size);
}